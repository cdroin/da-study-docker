FROM gitlab-registry.cern.ch/linuxsupport/cs9-base
LABEL author="colas.noe.droin@cern.ch"

RUN dnf -y install curl which bzip2 libgfortran git file man-db make \
    gcc gcc-gfortran python3-pip python3-devel wget vim locmap-release
    
RUN dnf -y install locmap
RUN locmap --enable kerberos
RUN locmap --enable afs
RUN locmap --enable eosclient
RUN locmap --configure kerberos; exit 0
RUN locmap --configure afs; exit 0
RUN locmap --configure eosclient; exit 0
RUN dnf -y install eos-xrootd xrootd xrootd-client

WORKDIR /usr/local/DA_study

RUN wget https://github.com/conda-forge/miniforge/releases/latest/download/Miniforge3-Linux-x86_64.sh
RUN bash Miniforge3-Linux-x86_64.sh -b -p /usr/local/DA_study/miniforge_docker
RUN rm Miniforge3-Linux-x86_64.sh

RUN source /usr/local/DA_study/miniforge_docker/bin/activate && \
    git config --global http.postBuffer 157286400 && \
    pip install matplotlib --no-cache-dir && \
    pip install numpy --no-cache-dir && \
    pip install scipy --no-cache-dir && \
    pip install pandas --no-cache-dir && \
    pip install ipython --no-cache-dir && \
    pip install psutil --no-cache-dir && \
    pip install cpymad --no-cache-dir && \
    pip install xsuite && \
    # Precompile kernel xsuite
    xsuite-prebuild regenerate

# Despite ocl-icd-system, pocl-binary-distribution seems to be needed, otherwise pyopencl won't work on Bologna
RUN source /usr/local/DA_study/miniforge_docker/bin/activate && \
    conda install compilers && \
    conda install gpustat && \

    # For simulation with opencl
    conda install ocl-icd-system && \
    pip install pyopencl && \
    pip install pocl-binary-distribution && \
    pip install mako && \

    # For simulation with cupy
    conda install mamba -n base -c conda-forge && \
    pip install cupy-cuda11x --no-cache-dir && \
    mamba install cudatoolkit=11.8.0 && \
    # Install nafflib at the end to ensure no incompatibility
    pip install nafflib --no-cache-dir

RUN source /usr/local/DA_study/miniforge_docker/bin/activate && \
    mkdir modules && \
    cd modules && \
    git clone https://github.com/xsuite/tree_maker.git && \
    python -m pip install -e tree_maker && \
    git clone https://gitlab.cern.ch/acc-models/acc-models-lhc.git -b hl16 hllhc16 && \
    # Get xtrack separately to have the lumi computation with the CC
    git clone https://github.com/ColasDroin/xtrack.git -b lumi_with_crabs && \
    pip install -e xtrack && \
    git clone https://github.com/xsuite/xmask.git && \
    pip install -e xmask && \
    cd xmask/ && \
    git submodule init && \
    git submodule update && \
    cd .. && \
    # Install study-DA at the end as it relies on Xsuite and other dependencies
    pip install study-da --no-cache-dir

# set environment variables for cupy
ENV CUPY_CACHE_DIR /tmp/cupy_cache
    
RUN echo "source miniforge_docker/bin/activate" >> ~/.bashrc
